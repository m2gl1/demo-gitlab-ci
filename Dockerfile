FROM openjdk:17
MAINTAINER ngor
ADD ./target/demo-gitlab-ci.jar /demo/demo-gitlab-ci.jar
EXPOSE 8086
ENTRYPOINT ["java","-jar","/demo/demo-gitlab-ci.jar"]
